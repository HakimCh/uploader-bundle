<?php

declare(strict_types=1);

namespace HakimCh\UploaderBundle\Constraint;

use HakimCh\UploaderBundle\Contract\ConstraintInterface;
use HakimCh\UploaderBundle\Contract\FileInterface;
use HakimCh\UploaderBundle\Exceptions\FileConstraintException;

class ExtensionConstraint implements ConstraintInterface
{
    /**
     * @var array
     */
    private $allowedExtensions;

    public function __construct(array $allowedExtensions)
    {
        $this->allowedExtensions = $allowedExtensions;
    }

    /**
     * @param FileInterface $file
     *
     * @throws FileConstraintException
     */
    public function validate(FileInterface $file): void
    {
        if (\in_array($file->getExtension(), $this->allowedExtensions)) {
            return;
        }
        throw new FileConstraintException(sprintf(
            '%s not allowed, please try with an allowed extension (%s)',
            $file->getExtension(),
            implode(', ', $this->allowedExtensions)
        ));
    }
}
