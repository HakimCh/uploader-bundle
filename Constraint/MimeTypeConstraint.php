<?php

declare(strict_types=1);

namespace HakimCh\UploaderBundle\Constraint;

use function explode;
use HakimCh\UploaderBundle\Contract\ConstraintInterface;
use HakimCh\UploaderBundle\Contract\FileInterface;
use HakimCh\UploaderBundle\Exceptions\FileConstraintException;
use function mime_content_type;

class MimeTypeConstraint implements ConstraintInterface
{
    /**
     * @var string
     */
    private $mimeType;

    /**
     * MimeTypeConstraint constructor.
     *
     * @param array|string $mimeType
     */
    public function __construct($mimeType)
    {
        $this->mimeType = \is_array($mimeType) ?: [$mimeType];
    }

    /**
     * @param FileInterface $file
     *
     * @throws FileConstraintException
     */
    public function validate(FileInterface $file): void
    {
        [$mimeType, ] = explode('/', mime_content_type($file->getPathname()));
        if (\in_array($mimeType, $this->mimeType)) {
            return;
        }
        throw new FileConstraintException(sprintf('%s is not an allowed file type', $mimeType));
    }
}
