<?php

declare(strict_types=1);

namespace HakimCh\UploaderBundle\Constraint;

use HakimCh\UploaderBundle\Contract\ConstraintInterface;
use HakimCh\UploaderBundle\Contract\FileInterface;
use HakimCh\UploaderBundle\Exceptions\FileConstraintException;

class SizeConstraint implements ConstraintInterface
{
    /**
     * @var int
     */
    private $size;

    public function __construct(int $size)
    {
        $this->size = $size;
    }

    /**
     * @param FileInterface $file
     *
     * @throws FileConstraintException
     */
    public function validate(FileInterface $file): void
    {
        if ($file->getSize() <= $this->size) {
            return;
        }
        throw new FileConstraintException(sprintf(
            'The file is too big, the maximum allowed is %d',
            $file->getSize()
        ));
    }
}
