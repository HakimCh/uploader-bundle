<?php

declare(strict_types=1);

namespace HakimCh\UploaderBundle\Entity;

use HakimCh\UploaderBundle\Contract\FileInterface;
use HakimCh\UploaderBundle\Exceptions\FileNotFoundException;

class File implements FileInterface
{
    /**
     * @var string
     */
    private $pathname;
    /**
     * @var string
     */
    private $filename;
    /**
     * @var string
     */
    private $basename;
    /**
     * @var string
     */
    private $extension;
    /**
     * @var string
     */
    private $path;

    /**
     * File constructor.
     *
     * @param string $name
     *
     * @throws FileNotFoundException
     */
    public function __construct(string $name)
    {
        if (!file_exists($name)) {
            throw new FileNotFoundException('File %s not found', $name);
        }
        [
            'dirname' => $this->path,
            'basename' => $this->basename,
            'extension' => $this->extension,
            'filename' => $this->filename,
        ] = pathinfo($name);
        $this->pathname = $name;
    }

    /**
     * @return string
     */
    public function getPathname(): string
    {
        return $this->pathname;
    }

    /**
     * @return string
     */
    public function getFilename(): string
    {
        return $this->filename;
    }

    /**
     * @return string
     */
    public function getBasename(): string
    {
        return $this->basename;
    }

    /**
     * @return string
     */
    public function getExtension(): string
    {
        return $this->extension;
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @return int
     */
    public function getSize(): int
    {
        return filesize($this->pathname);
    }
}
