<?php

declare(strict_types=1);

namespace HakimCh\UploaderBundle\Traits;

use HakimCh\UploaderBundle\Contract\FileInterface;

trait FileTrait
{
    /**
     * @param FileInterface $file
     * @param bool          $keepOriginalName
     *
     * @return string
     */
    public function getFilename(FileInterface $file, bool $keepOriginalName = false): string
    {
        $filename = preg_replace('/[^a-zA-Z0-9\-\._]/', '', $file->getFilename());
        $filename = str_replace(['.', '_'], '-', $filename);

        if (!$keepOriginalName) {
            $filename .= '-'.uniqid();
        }

        return strtolower(sprintf('%s.%s', $filename, $file->getExtension()));
    }

    /**
     * @param string $pathname
     * @param string $publicPath
     *
     * @return string
     */
    public function getRelativePathname(string $pathname, string $publicPath): string
    {
        return str_replace($publicPath, '', $pathname);
    }

    /**
     * @param string        $publicPath
     * @param string        $destination
     * @param FileInterface $file
     * @param bool          $keepOriginalName
     *
     * @return string
     */
    public function buildDestinationPathname(string $publicPath, string $destination, FileInterface $file, bool $keepOriginalName = false): string
    {
        if (!file_exists($destinationPath = $publicPath.'/'.$destination)) {
            mkdir($destinationPath, 0755, true);
        }

        return $destinationPath.'/'.$this->getFilename($file, $keepOriginalName);
    }
}
