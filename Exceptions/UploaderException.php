<?php

declare(strict_types=1);

namespace HakimCh\UploaderBundle\Exceptions;

use Exception;

class UploaderException extends Exception
{
}
