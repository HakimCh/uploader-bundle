<?php

declare(strict_types=1);

namespace HakimCh\UploaderBundle\Contract;

interface FileInterface
{
    public function getBasename();

    public function getFilename();

    public function getExtension();

    public function getPathname();

    public function getSize();
}
