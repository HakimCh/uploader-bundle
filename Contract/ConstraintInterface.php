<?php

declare(strict_types=1);

namespace HakimCh\UploaderBundle\Contract;

interface ConstraintInterface
{
    public function validate(FileInterface $file);
}
