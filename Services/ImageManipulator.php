<?php

declare(strict_types=1);

namespace HakimCh\UploaderBundle\Services;

use Imagine\Gd\Imagine;
use Imagine\Image\Box;
use Imagine\Image\ImageInterface;
use Imagine\Image\ManipulatorInterface;
use Imagine\Image\Point;

class ImageManipulator
{
    /**
     * @var ImageInterface
     */
    private $processor;
    /**
     * @var int
     */
    private $compressionLevel;

    public function __construct(int $compressionLevel = 9)
    {
        $this->compressionLevel = $compressionLevel;
    }

    /**
     * @param string $pathname
     */
    public function open(string $pathname): void
    {
        $this->processor = (new Imagine())->open($pathname);
    }

    /**
     * @param int $width
     */
    public function resizeByWidth(int $width): void
    {
        $this->processor = $this->processor->resize(
            $this->processor->getSize()->widen($width)
        );
    }

    /**
     * @param int $height
     */
    public function resizeByHeight(int $height): void
    {
        $this->processor = $this->processor->resize(
            $this->processor->getSize()->heighten($height)
        );
    }

    /**
     * @param int        $width
     * @param int        $height
     * @param int|string $settings
     */
    public function resize(int $width, int $height, $settings = ManipulatorInterface::THUMBNAIL_OUTBOUND): void
    {
        $this->processor = $this->processor->thumbnail(new Box($width, $height), $settings);
    }

    /**
     * @param int $width
     * @param int $height
     */
    public function autoResize(int $width, int $height)
    {
        $size = $this->processor->getSize();
        dump($size->getWidth() > $size->getHeight());
        if ($size->getWidth() > $size->getHeight()) {
            $estimatedHeight = $size->getHeight() * ($width / $size->getWidth());
            if ($estimatedHeight <= $height) {
                return $this->resizeByWidth($width);
            }
        }

        return $this->resizeByHeight($height);
    }

    /**
     * @param int $x
     * @param int $y
     * @param int $width
     * @param int $height
     */
    public function crop(int $x, int $y, int $width, int $height): void
    {
        $this->processor = $this->processor->crop(new Point($x, $y), new Box($width, $height));
    }

    /**
     * @param string $destinationPathname
     * @param string $extension
     */
    public function save(string $destinationPathname, string $extension): void
    {
        $options = [];
        switch ($extension) {
            case 'jpg':
            case 'jpeg':
                $options = ['jpeg_quality' => $this->compressionLevel * 10];
                break;
            case 'png':
                $options = ['png_compression_level' => $this->compressionLevel];
        }
        $this->processor->save($destinationPathname, $options);
    }
}
