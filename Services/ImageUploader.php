<?php

declare(strict_types=1);

namespace HakimCh\UploaderBundle\Services;

use Exception;
use HakimCh\UploaderBundle\Entity\File;

final class ImageUploader extends AbstractUploader
{
    public const RESIZE_THUMBNAIL = 0;
    public const RESIZE_HEIGHTEN = 1;
    public const RESIZE_WIDEN = 2;
    /**
     * @var bool
     */
    private $keepOriginalName;
    /**
     * @var ImageManipulator
     */
    private $manipulator;

    public function __construct(string $publicPath, $compressionLevel = 9, $keepOriginalName = true)
    {
        $this->publicPath = $publicPath;
        $this->keepOriginalName = $keepOriginalName;
        $this->manipulator = new ImageManipulator($compressionLevel);
    }

    /**
     * @param string $pathname
     *
     * @return ImageUploader
     */
    public function open(string $pathname): self
    {
        $this->manipulator->open($pathname);
        $this->file = new File($pathname);

        return $this;
    }

    /**
     * @param int $height
     *
     * @return ImageUploader
     */
    public function resizeByHeight(int $height): self
    {
        $this->manipulator->resizeByHeight($height);

        return $this;
    }

    /**
     * @param int $width
     *
     * @return ImageUploader
     */
    public function resizeByWidth(int $width): self
    {
        $this->manipulator->resizeByWidth($width);

        return $this;
    }

    /**
     * @param int  $width
     * @param int  $height
     * @param bool $adaptative
     *
     * @return ImageUploader
     */
    public function thumbnail(int $width, int $height, bool $adaptative = false): self
    {
        if ($adaptative) {
            $this->manipulator->autoResize($width, $height);
        } else {
            $this->manipulator->resize($width, $height);
        }

        return $this;
    }

    /**
     * @param int $x
     * @param int $y
     * @param int $width
     * @param int $height
     *
     * @return ImageUploader
     */
    public function crop(int $x, int $y, int $width, int $height): self
    {
        $this->processor->crop($x, $y, $width, $height);

        return $this;
    }

    /**
     * @param string $destination
     *
     * @throws Exception
     *
     * @return string
     */
    public function upload(string $destination): string
    {
        $this->validate($this->file);
        $destinationPathname = $this->buildDestinationPathname($this->publicPath, $destination, $this->file, $this->keepOriginalName);
        $this->manipulator->save($destinationPathname, $this->file->getExtension());

        return '/images'.$this->getRelativePathname($destinationPathname, $this->publicPath);
    }
}
