<?php

declare(strict_types=1);

namespace HakimCh\UploaderBundle\Services;

use Exception;
use HakimCh\UploaderBundle\Entity\File;
use HakimCh\UploaderBundle\Storage\AbstractStorage;

final class FileUploader extends AbstractUploader
{
    /**
     * @var AbstractStorage
     */
    protected $storage;

    public function __construct(string $publicPath, AbstractStorage $storage)
    {
        $this->publicPath = $publicPath;
        $this->storage = $storage;
    }

    /**
     * @param string $pathname
     *
     * @return FileUploader
     */
    public function open(string $pathname): self
    {
        $this->file = new File($pathname);

        return $this;
    }

    /**
     * @param string $destination
     *
     * @throws Exception
     *
     * @return string
     */
    public function upload(string $destination): string
    {
        $this->validate($this->file);
        $pathname = $this->storage->upload($this->file);

        return $this->getRelativePathname($pathname, $this->publicPath);
    }

    /**
     * @param string $destination
     */
    public function setDestination(string $destination): void
    {
        parent::setDestination($destination);
        $this->storage->setDestination($this->destination);
    }
}
