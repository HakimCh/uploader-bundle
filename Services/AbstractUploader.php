<?php

declare(strict_types=1);

namespace HakimCh\UploaderBundle\Services;

use Exception;
use HakimCh\UploaderBundle\Contract\ConstraintInterface;
use HakimCh\UploaderBundle\Contract\FileInterface;
use HakimCh\UploaderBundle\Traits\FileTrait;

abstract class AbstractUploader
{
    use FileTrait;

    /**
     * @var string
     */
    protected $publicPath;
    /**
     * @var ConstraintInterface[]
     */
    protected $constraints;
    /**
     * @var FileInterface
     */
    protected $file;

    /**
     * @param string $pathname
     *
     * @throws Exception
     *
     * @return string
     */
    abstract public function open(string $pathname);

    /**
     * @param string $destination
     *
     * @throws Exception
     *
     * @return string
     */
    abstract public function upload(string $destination): string;

    /**
     * @return string
     */
    public function getPublicPath(): string
    {
        return $this->publicPath;
    }

    /**
     * @param ConstraintInterface $constraint
     */
    public function addConstraint(ConstraintInterface $constraint)
    {
        $this->constraints[\get_class($constraint)] = $constraint;
    }

    /**
     * @param FileInterface $file
     *
     * @throws Exception
     */
    protected function validate(FileInterface $file): void
    {
        if (!empty($this->constraints)) {
            foreach ($this->constraints as $constraint) {
                $constraint->validate($file);
            }
        }
    }
}
