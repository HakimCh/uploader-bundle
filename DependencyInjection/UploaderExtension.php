<?php

declare(strict_types=1);

namespace HakimCh\UploaderBundle\DependencyInjection;

use Exception;
use HakimCh\UploaderBundle\Services\ImageUploader;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\ExtensionInterface;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

class UploaderExtension implements ExtensionInterface
{
    /**
     * @param array            $configs
     * @param ContainerBuilder $container
     *
     * @throws Exception
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $loader = new YamlFileLoader(
            $container,
            new FileLocator(__DIR__.'/../Resources/config')
        );

        $loader->load('services.yaml');

        if (isset($configs[0]['image'])) {
            $definition = $container->getDefinition(ImageUploader::class);
            $definition->setArguments([
                '$publicPath' => $configs[0]['public_path'].$configs[0]['image']['folder_name'],
                '$compressionLevel' => $configs[0]['image']['compression_level'],
            ]);
        }
    }

    public function getAlias()
    {
        return 'uploader';
    }

    public function getXsdValidationBasePath()
    {
        // TODO: Implement getXsdValidationBasePath() method.
    }

    public function getNamespace()
    {
        // TODO: Implement getNamespace() method.
    }
}
