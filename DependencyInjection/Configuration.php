<?php

declare(strict_types=1);

namespace HakimCh\UploaderBundle\DependencyInjection;

use HakimCh\UploaderBundle\Storage\FileSystem;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('uploader');

        $treeBuilder
            ->getRootNode()
            ->children()
                ->scalarNode('public_path')->isRequired()->end()
                ->scalarNode('storage_class')->defaultValue(FileSystem::class)->end()
                ->scalarNode('max_upload_size')->defaultValue(FileSystem::getMaximumUploadSize())->end()
                ->arrayNode('image')
                    ->defaultValue([])
                    ->children()
                        ->scalarNode('folder_name')->end()
                        ->scalarNode('max_upload_size')->end()
                        ->scalarNode('compression_level')->end()
                    ->end()
                ->end()
            ->end();

        return $treeBuilder;
    }
}
