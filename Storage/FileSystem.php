<?php

declare(strict_types=1);

namespace HakimCh\UploaderBundle\Services\Storage;

use HakimCh\UploaderBundle\Contract\FileInterface;
use HakimCh\UploaderBundle\Exceptions\UploaderException;
use HakimCh\UploaderBundle\Storage\AbstractStorage;
use InvalidArgumentException;

class FileSystem extends AbstractStorage
{
    /**
     * @param FileInterface $file
     * @param bool          $keepOriginalName
     *
     * @throws UploaderException
     *
     * @return string
     */
    public function upload(FileInterface $file, bool $keepOriginalName = false): string
    {
        if (!is_dir($this->destination)) {
            throw new InvalidArgumentException('Directory does not exist');
        }
        if (!is_writable($this->destination)) {
            throw new InvalidArgumentException('Directory is not writable');
        }

        $path = $this->destination.$this->getFilename($file, $keepOriginalName);
        if (!$this->overwrite && file_exists($path)) {
            throw new UploaderException(sprintf('File %s already exists', $file->getBasename()));
        }
        if (!move_uploaded_file($file->getPathname(), $path)) {
            throw new UploaderException(sprintf('File %s could not be moved to final destination', $file->getBasename()));
        }

        return $path;
    }

    public static function getMaximumUploadSize(): int
    {
        $max_upload = min(ini_get('post_max_size'), ini_get('upload_max_filesize'));
        $max_upload = str_replace('M', '', $max_upload);

        return $max_upload * 1024;
    }
}
