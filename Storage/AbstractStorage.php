<?php

declare(strict_types=1);

namespace HakimCh\UploaderBundle\Storage;

use HakimCh\UploaderBundle\Contract\FileInterface;
use HakimCh\UploaderBundle\Traits\FileTrait;

abstract class AbstractStorage
{
    use FileTrait;

    /**
     * @var string
     */
    protected $destination;
    /**
     * @var bool
     */
    protected $overwrite = false;

    /**
     * @param FileInterface $file
     * @param bool          $keepOriginalName
     *
     * @return string
     */
    abstract public function upload(FileInterface $file, bool $keepOriginalName = false): string;

    /**
     * @return int
     */
    abstract public static function getMaximumUploadSize(): int;

    /**
     * @param string $destination
     */
    public function setDestination(string $destination): void
    {
        $this->destination = $destination;
    }

    /**
     * @param string $overwrite
     */
    public function setOverwrite(string $overwrite): void
    {
        $this->overwrite = $overwrite;
    }
}
